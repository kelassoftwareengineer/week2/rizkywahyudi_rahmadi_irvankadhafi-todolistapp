/*
 * This Java source file was generated by the Gradle 'init' task.
 */

import static org.junit.Assert.*;

import mytodoapp.Task;
import org.junit.Test;

public class TaskTest {
    @Test
    public void testCreateTask() {
        Task task = new Task("Belajar TDD", 'I');
        assertEquals("Belajar TDD", task.getName());
        assertEquals('I', task.getStatus());
    }

    @Test
    public void testCreateTask2() {
        Task task = new Task("Belajar TDD", 'I');
        assertEquals("Belajar TDD", task.getName());
        assertEquals('I', task.getStatus());
        task.setStatus('C');
        assertEquals('C', task.getStatus());
    }
}
