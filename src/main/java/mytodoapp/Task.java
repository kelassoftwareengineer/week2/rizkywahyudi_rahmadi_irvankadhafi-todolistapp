package mytodoapp;

public class Task {

    private String name;
    private char status;

    public Task(String name, char status) {
        this.name = name;
        this.status = status;
    }

    public String getName() {
        return this.name;
    }

    public char getStatus() {
        return this.status;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setStatus(char status) {
        this.status = status;
    }
}
