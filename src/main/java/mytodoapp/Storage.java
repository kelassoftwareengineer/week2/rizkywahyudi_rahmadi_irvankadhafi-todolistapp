package mytodoapp;

import java.util.ArrayList;
import java.util.List;

public class Storage {

    private List<Task> taskList = new ArrayList<>();

    public void store(Task task) {
        taskList.add(task);
    }

    public void retrieve() {
        int taskNumber = 1;
        for (Task task : taskList) {
            System.out.println("\nTask " + taskNumber);
            System.out.println(task.getName());
            System.out.println("Status : " + task.getStatus());
            taskNumber++;
        }
    }

    public int count() {
        return this.taskList.size();
    }
}
